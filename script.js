document.addEventListener("DOMContentLoaded", function() {
    const button = document.getElementById("button");
    const div = document.getElementById("div");

    button.addEventListener("click", function() {
        div.textContent = "Зміна...";
    
        setTimeout(function() {
            div.textContent = "Операція виконана успішно!";
        }, 3000); 
    });
});


// task 2


document.addEventListener("DOMContentLoaded", function() {
    const clock = document.getElementById("time");
    let count = 10;

    const countInterval = setInterval(function() {
        if (count > 0) {
            clock.textContent = count;
            count--;
        } else {
            clearInterval(countInterval);
            clock.textContent = "Зворотній відлік завершено";
        }
    }, 1000); 
});